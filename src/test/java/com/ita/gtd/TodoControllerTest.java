package com.ita.gtd;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita.gtd.entity.Todo;
import com.ita.gtd.repository.TodoRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    private MockMvc client;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void initRepository(){
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todos_perform_get_given_todos_in_db() throws Exception {
        Todo readBooks = new Todo("read books" , false);
        Todo doSport = new Todo("do sport" , false);

        todoRepository.saveAll(List.of(readBooks, doSport));

        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(readBooks.getText()))
                .andExpect(jsonPath("$[0].done").value(readBooks.getDone()))
                .andExpect(jsonPath("$[1].text").value(doSport.getText()))
                .andExpect(jsonPath("$[1].done").value(doSport.getDone()));
    }

    @Test
    void should_return_todo_perform_get_given_id() throws Exception {
        Todo readBooks = new Todo("read books" , false);
        Todo savedTodo = todoRepository.save(readBooks);


        client.perform(MockMvcRequestBuilders.get("/todos/{id}", savedTodo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(readBooks.getId()))
                .andExpect(jsonPath("$.text").value(readBooks.getText()))
                .andExpect(jsonPath("$.done").value(readBooks.getDone()));
    }

    @Test
    void should_return_todo_perform_post_given_todo_info() throws Exception {

        Todo readBooks = new Todo("read books" , false);

        client.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(readBooks)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").value(readBooks.getText()))
                .andExpect(jsonPath("$.done").value(readBooks.getDone()));
    }

    @Test
    void should_return_update_todo_perform_put_given_todo_info() throws Exception {
        Todo readBooks = new Todo("read books" , false);
        Todo doSport = new Todo("do sport", true);

        Todo saveTodo = todoRepository.save(readBooks);

        client.perform(MockMvcRequestBuilders.put("/todos/{id}", saveTodo.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(doSport)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(doSport.getText()))
                .andExpect(jsonPath("$.done").value(doSport.getDone()));
    }

    @Test
    void should_delete_todo_perform_delete_given_id() throws Exception {
        Todo readBooks = new Todo("read books" , false);
        Todo saveTodo = todoRepository.save(readBooks);

        client.perform(MockMvcRequestBuilders.delete("/todos/{id}", saveTodo.getId()))
                .andExpect(status().isNoContent());

        assertNull(todoRepository.findById(saveTodo.getId()).orElse(null));
    }


    @Test
    void should_return_404_when_perform_get_given_id_not_exist() throws Exception {
        client.perform(MockMvcRequestBuilders.get("/todos/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
