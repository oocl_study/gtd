DROP TABLE IF EXISTS todo;

CREATE TABLE `todo`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NULL,
  `done` tinyint(0) NULL,
  PRIMARY KEY (`id`)
);