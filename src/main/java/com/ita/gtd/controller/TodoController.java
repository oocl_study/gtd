package com.ita.gtd.controller;

import com.ita.gtd.dto.TodoRequest;
import com.ita.gtd.dto.TodoResponse;
import com.ita.gtd.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping
    public List<TodoResponse> getTodos(){
        return todoService.getTodos();
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable("id") Integer id){
        return todoService.getTodoById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public TodoResponse addTodo(@RequestBody TodoRequest todoRequest){
        return todoService.addTodo(todoRequest);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable("id") Integer id, @RequestBody TodoRequest todoRequest){
        return todoService.updateTodo(id, todoRequest);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteTodo(@PathVariable("id") Integer id){
        todoService.deleteTodo(id);
    }
}
