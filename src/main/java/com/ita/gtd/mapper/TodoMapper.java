package com.ita.gtd.mapper;

import com.ita.gtd.dto.TodoRequest;
import com.ita.gtd.dto.TodoResponse;
import com.ita.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class TodoMapper {

    public static List<TodoResponse> convertTodoToTodoResponse(List<Todo> todos) {
        return todos.stream().map(todo -> {
            TodoResponse todoResponse = new TodoResponse();
            BeanUtils.copyProperties(todo, todoResponse);
            return todoResponse;
        }).collect(Collectors.toList());
    }

    public static TodoResponse convertTodoToTodoResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static Todo convertTodoRequestToTodo(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest, todo );
        return todo;
    }
}
