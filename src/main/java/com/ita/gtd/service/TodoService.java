package com.ita.gtd.service;

import com.ita.gtd.advice.TodoNotFoundException;
import com.ita.gtd.dto.TodoRequest;
import com.ita.gtd.dto.TodoResponse;
import com.ita.gtd.entity.Todo;
import com.ita.gtd.mapper.TodoMapper;
import com.ita.gtd.repository.TodoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    public List<TodoResponse> getTodos() {
        List<Todo> todos = todoRepository.findAll();
        return TodoMapper.convertTodoToTodoResponse(todos);
    }


    public TodoResponse getTodoById(Integer id) {
        Optional<Todo> todoOptional = todoRepository.findById(id);
        Todo todo = todoOptional.orElse(null);
        if(todo == null) throw new TodoNotFoundException();
        return TodoMapper.convertTodoToTodoResponse(todo);
    }


    public TodoResponse addTodo(TodoRequest todoRequest) {
        Todo todo = todoRepository.save(TodoMapper.convertTodoRequestToTodo(todoRequest));
        return TodoMapper.convertTodoToTodoResponse(todo);
    }

    public TodoResponse updateTodo(Integer id, TodoRequest todoRequest) {
        Todo todo = todoRepository.findById(id).orElse(null);
        if (todo == null) throw new TodoNotFoundException();

        todo.setText(todoRequest.getText());
        todo.setDone(todoRequest.getDone());

        todoRepository.save(todo);
        return TodoMapper.convertTodoToTodoResponse(todo);
    }

    public void deleteTodo(Integer id) {
        todoRepository.deleteById(id);
    }
}
